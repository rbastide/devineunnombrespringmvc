package devine.sessiondata;


import java.io.Serializable;
import java.util.Random;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Variable de session représentant les infos du jeu en cours
 */
@Component
@SessionScope
public class GameInfo implements Serializable {
	private static final long serialVersionUID = 7557337120605349516L;
	private static final int MAX = 100;
	// Un générateur aléatoire
	Random generator = new Random(System.nanoTime());
	
	// Le nombre à deviner
	private int target = -1;
	// Le nombre d'essais
	private int attempts = 0;
	// La dernière proposition enregistrée
	private int guess = 0;
	
	/**
	 * Démarre un nouveau jeu
	 */
	public void start() {
		target = generator.nextInt(MAX + 1);
		attempts = 0;
		guess = -1;
	}

	/**
	 * Terminer le jeu en cours
	 */
	public void finishGame() {
		this.target = -1 ;
		this.attempts = 0;	
		guess = 0;
	}
	
	/**
	 * @return vrai si le jeu est fini
	 */
	public boolean isFinished() {
		return guess == target;
	}
	
	public int getAttempts() {
		return attempts;
	}
	
	/**
	 * Enregistre une nouvelle proposition
	 * @param guess la nouvelle proposition
	 */
	public void newGuess(int guess) {
		attempts++;
		this.guess = guess;
	}
	
	// Accesseurs
	public int getGuess() {
		return guess;
	}

	public int getTarget() {
		return target;
	}
	
	public int getMax() {
		return MAX;
	}
}
