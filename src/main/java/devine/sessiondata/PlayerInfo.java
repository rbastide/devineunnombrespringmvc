package devine.sessiondata;


import java.io.Serializable;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Variable de session représentant les infos du joueur
 */
@Component
@SessionScope
public class PlayerInfo implements Serializable {
	
	private static final long serialVersionUID = 7445124270947194875L;

	private String name = null;
	
	public void login(String name) {
		this.name = name;
	}
	public void logout() {
		this.name = null;
	}
	public boolean isConnected() {
		return name != null;
	}
	public String getName() {
		return name;
	}
}
