package devine.controller;

import devine.applicationdata.BestScoreInfo;
import devine.sessiondata.GameInfo;
import devine.sessiondata.PlayerInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/") // This means URL's start with / (after Application path)
public class DevineController {
	Logger logger = LoggerFactory.getLogger(DevineController.class);
	
	// Meilleur score, Application scoped (partagé par tous les utilisateurs)
	@Autowired // Initialisé et injecté automatiquement par Spring
	private BestScoreInfo best;

	// Les infos du joueur, Session scoped (propre à chaque utilisateurs)
	@Autowired // Initialisé et injecté automatiquement par Spring
	private PlayerInfo player;

	// Les infos du jeu en cours, Session scoped (propre à chaque utilisateurs)
	@Autowired // Initialisé et injecté automatiquement par Spring
	private GameInfo game;

	// Pour chaque mapping, on ajoute les informations nécessaires pour les afficher dans les vues
	// Equivalent à : model.addAtribute("player", player);
	@ModelAttribute("player")
	PlayerInfo getPlayer() {
		return player;
	}
	
	@ModelAttribute("game")
	GameInfo getGame() {
		return game;
	}	
	
	@ModelAttribute("best")
	BestScoreInfo getBestScore() {
		return best;
	}
	
	/**
	 * En fonction de l'état de l'application, on affiche la vue appropriée
	 * @return le nom de la vue à afficher
	 **/
	@GetMapping
	public String showView() {
		if (!player.isConnected()) { // Pas encore connecté
			return "loginForm"; // On montre la page de login
		}

		if (game.isFinished()) {  // On vient de gagner
			return "winGame";// On dit bravo
		}
		// Le joueur est connecté, le jeu n'est pas fini, on continue à jouer
		return "guessNumber"; // On montre le formulaire pour proposer un nombre
	}
	
	/**
	 * Appelé par la vue "guessNumber" quand on a choisi un nombre.
	 * Exemple de "POST - Redirect - GET"
	 * @param guess le nombre choisi
	 * @return une redirection vers le mapping "/"
	 */
	@PostMapping(path = "guess")
	public String guessNumber(@RequestParam int guess) {
		if (player.isConnected()) {
			logger.debug("Le joueur {} propose {}", player.getName(), guess);		
			game.newGuess(guess);
			if (game.isFinished()) { 
				logger.debug("Le joueur {} a trouvé la solution !", player.getName());		
				best.updateBestScore(player, game); // On met à jour le meilleur score
				logger.debug("Le meilleur score est maintenant: {}", best.getBestScore());		
			}
		}
		return "redirect:/";
	}	
	
	/**
	 * Appelé par la méthode POST via le formulaire de connection "loginForm"
	 * @param playerName le nom choisi par le joueur
	 * @return une redirection vers le mapping "/" après avoir initialisé un nouveau jeu
	 */
	@PostMapping(path = "login")
	public String login(@RequestParam String playerName) {
		player.login(playerName);
		logger.debug("Le joueur {} vient de se connecter", player.getName());
		return playNewGame();
	}
	
	/**
	 * Appelé par un lien, quand l'utilisateur choisit de se déconnecter (vue "guessNumber")
	 * @return une redirection vers le mapping "/" 
	 */
	@GetMapping(path = "logout")
	public String logout() {
		logger.debug("Le joueur {} se déconnecte", player.getName());
		player.logout();
		return "redirect:/";
	}	
	
	/**
	 * Appelé par un lien, quand l'utilisateur choisit de faire une nouvelle partie  (vue "guessNumber")
	 * @return une redirection vers le mapping "/" 
	 */
	@GetMapping(path = "playAgain")
	public String playNewGame() {
		if (player.isConnected()) {
			game.start();
		}
		return "redirect:/";
	}
}
