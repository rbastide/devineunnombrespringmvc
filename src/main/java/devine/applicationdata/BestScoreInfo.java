package devine.applicationdata;

import devine.sessiondata.GameInfo;
import devine.sessiondata.PlayerInfo;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Données d'application, accessibles à tous les utilisateurs
 * Les infos sur le meilleur score : nombre minimun d'essais, nom du joueur
 */
@Component
@ApplicationScope
public class BestScoreInfo {	
	private String playerName = null;
	
	private int bestScore = Integer.MAX_VALUE;

	/**
	 * Le meilleur score enregistré pour l'application
	 * @return le meilleur score
	 */
	public int getBestScore() {
		return bestScore;
	}

	/**
	 * Le nom du joeur qui a fait le meilleur score
	 * @return le nom du détenteur du meilleur score, ou null si peronne
	 * n'a encore terminé la partie
	 */
	public String getPlayerName() {
		return playerName;
	}
	
	/**
	 * Mettre à jour si nécessaire les infos du meilleur score
	 * @param player le joueur qui vient de terminer la partie
	 * @param game   les infos sur la partie qui vient de se terminer
	 */
	public void updateBestScore(PlayerInfo player, GameInfo game) {
		if (game.getAttempts() < bestScore) { // player a battu le meilleur score
			// On met à jour les infos sur le meilleur score
			bestScore = game.getAttempts();
			playerName = player.getName();
		}
	}

}
